# @datetime: 7/26 0026
# @author: hanyaning@deri.energy
# @description: 自定义mysql工具的测试
import unittest

import pymysql

from util import logger,commonUtil,mysqlUtil
Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()


class mysqlUtil_test(unittest.TestCase):



    def test_executor(self):
        my_data_base = mysqlUtil.MyMysql(user="root",password="",
                                         database="koala_test",port=3306,charset="utf8",host="127.0.0.1")
        executor = mysqlUtil.SqlExecutor(my_data_base)
        table_name = "koala"
        self.assertEqual(executor.execute("select * from koala"),1)
        # 增
        new_user = {"name":"hhh","age":1}
        executor.insert(table_name,new_user)
        executor.commit()
        # 删
        executor.delete(table_name," where name='hyn'")
        executor.commit()
        # 改
        new_user["age"]  = 2
        executor.update(table_name,new_user,condition=" where name='hhh'")
        executor.commit()
        # 查
        Logger.info(f"{executor.select(table_name)}")

if __name__ == "__main__":
    unittest.main()