# @datetime: 7/19 0019
# @author: hanyaning@deri.energy
# @description: commonUtil模块工具的测试
import unittest

from util import commonUtil


class commonUtilTest(unittest.TestCase):

    def test_isBlank(self):
        self.assertEqual(commonUtil.isBlank("")
                         and commonUtil.isBlank("   ")
                         and commonUtil.isBlank({})
                         and commonUtil.isBlank([])
                         and commonUtil.isBlank(None)
                         , True)

    def test_isNotBlank(self):
        self.test_isBlank()

    def test_narrow_data_by_dichotomy(self):
        dict_a = {"name": "hyn", "age": 24}
        dict_b = {"name": "hyn", "age": 24}
        dict_c = {"name": "wcl", "age": 23}
        dict_d = {"name": "hyn", "age": 24}
        dict_e = {"name": "baby", "age": 0}
        dict_f = {"name": "hyn", "age": 24}
        dict_a1 = {"name": "hyn", "age": 24}
        dict_b2 = {"name": "hyn", "age": 24}
        dict_c1 = {"name": "wcl", "age": 23}
        dict_d2 = {"name": "hyn", "age": 24}
        dict_e1 = {"name": "baby", "age": 0}
        dict_f2 = {"name": "hyn", "age": 24}

        list = []
        list.append(dict_a)
        list.append(dict_b)
        list.append(dict_c)
        list.append(dict_d)
        list.append(dict_e)
        list.append(dict_f)
        list.append(dict_a1)
        list.append(dict_b2)
        list.append(dict_c1)
        list.append(dict_d2)
        list.append(dict_e1)
        list.append(dict_f2)
        self.assertEqual(commonUtil.narrow_data_by_dichotomy(list, "name", "hyn"), [{"name": "hyn", "age": 24}] * 8)
        self.assertEqual(commonUtil.narrow_data_by_dichotomy(list, "name", "zhangsan"), [])

    def test_formatEquationToDict(self):
        equaltion = "name=hyn &age =23& "
        result = {"name": "hyn", "age": str(23)}
        self.assertEqual(commonUtil.formatEquationToDict(equaltion, separate="&"), result)

    def test_get_file_name(self):
        filepath = "W:/code/git/myProject/pythonStudyRoad/study/dasktop_ui/wx_study.py"
        self.assertEqual("wx_study", commonUtil.get_file_name(filepath, separate="/"))

    def test_get_file_suffix(self):
        filepath = "W:/code/git/myProject/pythonStudyRoad/study/dasktop_ui/wx_study.py"
        self.assertEqual("py", commonUtil.get_file_suffix(filepath))


if __name__ == "__main__":
    unittest.main()
