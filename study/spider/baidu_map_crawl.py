# @datetime: 7/19 0019
# @author: hanyaning@deri.energy
# @description:
import os

from util import excelUtil
from util.crawlUtil import *

Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()

if __name__ == "__main__":
    downloader = HtmlDownloader(delay=1,turn_to_json = True)
    str = """
                Host: api.map.baidu.com
                Connection: keep-alive
                Cache-Control: max-age=0
                Upgrade-Insecure-Requests: 1
                User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
                Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3
                Accept-Encoding: gzip, deflate
                Accept-Language: zh-CN,zh;q=0.9
                Cookie: BAIDUID=32C95434BCDB5CADF220D009FE22ADB4:FG=1; PSTM=1563501754; BDSFRCVID=gbKOJeC62iPTHbOw-evsqhG_MrpIK4vTH6aoyrwZk7PoeaEMjP25EG0Pef8g0Kub-qhEogKKL2OTHm_F_2uxOjjg8UtVJeC6EG0P3J; H_BDCLCKID_SF=tJkOoD_KJC03ebjYh-rB5t6HbpoK2tbfbRAX3buQ2tnr8pcNLTDK2h-IjPOB-Kr-y6nDBp37BR6bqCJ1KlO1j4_ehb-Jbx3CtGFJoqrmBI-2sp5jDh31b6ksD-Rt5q34KKny0hvcBIocShnzBUjrDRLbXU6BK5vPbNcZ0l8K3l02VKO_e6KMejjLDNLqq-kX2D5MLRj85bbHjJRYq4bohjPs-4R9BtQmJJr0bUbdKq7bKJ5CKnQsQp4IKl3AB53qQg-q3R7X-t3AjI3MhPoHQfuq0aKt0x-jLIThVn0MW-5DKtjdQtnJyUPTD4nnBPrd3H8HL4nv2JcJbM5m3x6qLTKkQN3TJMIEK5r2SC_XtCKh3D; delPer=0; ZD_ENTRY=empty; BDUSS=0Q2QVVPQkx-UTlaOHE3azBrUW4ybEJtd1N5TTJrUjd4aXNtVXBYRnlGWUp6RmhkSVFBQUFBJCQAAAAAAAAAAAEAAABvD~IqaHlua29hbGEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAk~MV0JPzFdd; BDRCVFR[n9IS1zhFc9f]=mk3SLVN4HKm; PSINO=3; H_PS_PSSID=; BIDUPSID=26824223E79DF449ECCABDF383D6B6E0; BDORZ=FFFB88E999055A3F8A630C64834BD6D0
        """
    header = crawUtils.chrome_header_parse(str)
    downloader.request_header = header
    page_size = 20
    page_num = 0
    params = {"query": "充电站", "page_size": page_size, "page_num": page_num, "output": "json","region":"南京",
              "ak": "zEDBVBYwlX2fPWnn6NoksbtLtHXnxE8O"}
    url = "http://api.map.baidu.com/place/v2/search"
    response = downloader.download(url=url, type="get", data=params)
    total = response["total"]
    Logger.info(total)
    result = []
    page_num = 0
    downloader.delay = 2
    while page_num * page_size < total:
        params["page_num"] = page_num
        response = downloader.get(url, params=params)
        for data in response["results"]:
            data["lat"] = data["location"]["lat"]
            data["lng"] = data["location"]["lng"]
            result.append(data)
        page_num+=1
    excel_Saver = excelUtil.ExcelSaver(to_path=os.path.join(os.getcwd(), "result"), file_name="station_excel")
    excel_Saver.saveDictArray(result)
    Logger.info(len(result))
