# @datetime: 7/18 0018
# @author: hanyaning@deri.energy
# @description:

# 爬虫调度程序
import json
import os
import time

import requests

from util import logger, commonUtil, excelUtil, crawlUtil


Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()

if __name__ == "__main__":
    # root_url = "https://baike.baidu.com/item/Python/407313"
    # csdn博客：https://blog.csdn.net/xing851483876/article/details/81842329
    header = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763",
        "Content-Type": "application/json",
        "Referer": "http://meishi.meituan.com/i/?ci=55&stid_b=1&cevent=imt%2Fhomepage%2Fcategory1%2F1",
    }

    cookie = commonUtil.formatEquationToDict(
        "__mta=46972568.1563441027015.1563442740316.1563501973643.6; client-id=5052bae8-32b6-4100-87d4-316ea19ecf4b; logan_session_token=sicl9bvbb99u797yoyb1; logan_custom_report=; __utmz=74597006.1563498241.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); cityname=%E5%8D%97%E4%BA%AC; _hc.v=2471a12a-e983-d691-c7a3-24a7d078c6ca.1563430420; latlng=31.966159%2C118.744847%2C1563498239598; _lxsdk_cuid=16c03b6fe248c-0af188a0413a8b-784a5935-1fa400-16c03b6fe25c8; webp=1; ci=55; __utma=74597006.260246837.1563498241.1563498241.1563498241.1; _lxsdk=290E5D987F718688794D2BFE5790B0603A06944090EF3A0A3FD48A8D577DC39A; iuuid=290E5D987F718688794D2BFE5790B0603A06944090EF3A0A3FD48A8D577DC39A; i_extend=H__a100005__b1; meishi_ci=55; uuid=597bf77b-b65a-4333-98f9-ef386094136b; _lxsdk_s=16c07fad042-8d-9da-010%7C%7C9"
    )
    Logger.info(cookie)
    root_url = "https://meishi.meituan.com/i/api/channel/deal/list"
    # root_url = "https://i.meituan.com/s/suggest.json?keyword=%E7%83%AD%E5%B9%B2%E9%9D%A2"
    # header = {'Host': 'meishi.meituan.com',
    #           'Accept': 'application/json',
    #           'Accept-Encoding': 'gzip, deflate, br',
    #           'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8',
    #           'Referer': 'https://meishi.meituan.com/i/?ci=30&stid_b=1&cevent=imt%2Fhomepage%2Fcategory1%2F1',
    #           'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Mobile Safari/537.36',
    #           "Cookie": "PHPSESSID=gv4hu6smem54dm5gf9l98mt5n0; Hm_lvt_f66b37722f586a240d4621318a5a6ebe=1563429354; Hm_lpvt_f66b37722f586a240d4621318a5a6ebe=1563429355; uuid=07a560d6379548779bc2.1563429367.1.0.0; _lx_utm=utm_source%3DBaidu%26utm_medium%3Dorganic; _lxsdk_cuid=16c03a6e756c8-0723fa86029ab6-3b654406-1fa400-16c03a6e756c8; _hc.v=4fc493ad-51e9-c93b-6f38-d45f19ef7484.1563430342; lat=32.038061; lng=118.792115; ci=55; rvct=55%2C30; JSESSIONID=2xbjoi7mwvfy1m0lt3unrsrwj; IJSESSIONID=2xbjoi7mwvfy1m0lt3unrsrwj; iuuid=9CC2E9D29B095160C10EE4859DE5BF3EF8140B8D141BEF7B9912AB72378FAAC9; cityname=%E5%8D%97%E4%BA%AC; _lxsdk=9CC2E9D29B095160C10EE4859DE5BF3EF8140B8D141BEF7B9912AB72378FAAC9; webp=1; __utma=74597006.1770781915.1563431435.1563431435.1563431435.1; __utmc=74597006; __utmz=74597006.1563431435.1.1.utmcsr=blog.csdn.net|utmccn=(referral)|utmcmd=referral|utmcct=/xing851483876/article/details/81842329; ci3=1; __mta=151812742.1563431435130.1563431435130.1563431451721.2; latlng=31.966157,118.744845,1563431452168; i_extend=C_b1Gimthomepagecategory11H__a; meishi_ci=55; _lxsdk_s=16c03a6e757-c47-d94-283%7C%7C89"}
    # # obj_spider = SpiderMain(header = header)
    #
    # # obj_spider.craw(root_url)
    data = {"uuid": "725df5d8-061e-4c75-9a11-d80ded3d90a5", "version": "8.3.3", "platform": 3, "app": "",
            "partner": 126, "riskLevel": 1, "optimusCode": 10,
            "originUrl": "http://meishi.meituan.com/i/?ci=55&stid_b=1&cevent=imt%2Fhomepage%2Fcategory1%2F1",
            "offset": 0, "limit": 15, "cateId": 21404, "lineId": 0, "stationId": 0, "areaId": 1174, "sort": "avgscore",
            "deal_attr_23": "", "deal_attr_24": "", "deal_attr_25": "", "poi_attr_20043": "", "poi_attr_20033": ""}
    # p = {'https': 'https://27.157.76.75:4275'}
    downloader = crawlUtil.HtmlDownloader(request_header=header, request_cookies=cookie)
    response = downloader.download(url=root_url, type="post", data=data)
    if response:
        result = []
        response_data = json.loads(response)
        Logger.info(response_data)

        total = response_data["data"]["poiList"]["totalCount"]
        offset = 24
        i = 0
        while offset < total:
            data["offset"] = offset
            time.sleep(5)
            try:
                Logger.info("开始第%s次爬取,data为：%s", i + 1, data)

                downloader = crawlUtil.HtmlDownloader(request_header=header, proxies=None, request_cookies=cookie)
                response = downloader.download(url=root_url, type="post", data=data)
                response_data = json.loads(response)
                pois = response_data["data"]["poiList"]["poiInfos"]
            except:
                Logger.info("第%s次失败了", i)
                break
            result.extend(pois)
            i += 1

            offset = 25 * i
        excel_saver = excelUtil.ExcelSaver(os.path.join(os.getcwd(), "result"), file_name="result")
        excel_saver.saveDictArray(result)
        Logger.info(len(result))
