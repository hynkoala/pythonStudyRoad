# @datetime:7/17 0017
# @author:"hanyaning@deri.energy"
# @description: 自定义类装饰器，实现调用方法时记录日志的功能
import time

import redis

from util import logger, commonUtil
from util.logDecorator import Log

Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()
Redis_conn = redis.Redis(host="127.0.0.1", port="6371")


@Log.log()
def do_something(who, msg=" hello koala"):
    begin = time.time()
    end = time.time()
    Logger.info(end - begin)
    return "hi " + who + msg


s = do_something("wangchengling")
Logger.info(s)

"""
将@log放到方法do_something前面，相当于执行了 do_something = log(do_something)
可以实现带参数的装饰器，带参的方法必须包在参数为被装饰方法的外面一层
可用三层/两层结构来概括：
    外：带参数的装饰器函数，用于传入自定义逻辑参数（可有可无）；
    中：参数名为被装饰函数名，直接return内部的方法；
    内：参数为被装饰函数的传参，此时已经获取到了自定义装饰器参数（如果有），调用方法名，调用参数，应有尽有
装饰器也可以用在类上，调用方法名为类名
!!需要注意，如果使用三层结构定义，使用时：@log(args);如果是两层结构，则调用时为：@log
"""
