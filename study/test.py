# @datetime: 7/26 0026
# @author: hanyaning@deri.energy
# @description:
from util import logger, commonUtil
from util import mysqlUtil

Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()

my_data_base = mysqlUtil.MyMysql(user="koala", password="koala", database="test")
executor = mysqlUtil.SqlExecutor(my_data_base)
# 查
query_user_sql = "select * from user"
# 获取所有查询记录
users = executor.query(query_user_sql)
# 查询单条数据用fetchone()
Logger.info(users)
