# @datetime: 7/18 0018
# @author: hanyaning@deri.energy
# @description: 正则表达式相关学习积累

import re

# 1、匹配手机号
phone = "15719541801"
# re_phone = r"^1[3-9][0-9]{9}$"
re_phone = r"^1[3-9]\d{9}$"
# print(re.match(re_phone,phone).groups())
# print(re.match(re_phone,phone).group())
# print(re.match(re_phone,phone).end())
"""
^表示开头，$表示结尾，\d与[0-9]相同，后跟{n}表示出现n次
"""

# 2、匹配邮箱
email = 'hynkoala@deri.energy'
re_email = r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9]+\.[a-zA-Z0-9.]+$"
print(re.match(re_email, email))
"""
.表示匹配任意字符，若要使用.作为字符去匹配，需要加\
+表示匹配前面表达式一次或者多次，*表示匹配零次或者多次
"""
