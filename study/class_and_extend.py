# @datetime: 7/17 0017
# @author: hanyaning@deri.energy
# @description: 类与继承探索
from util import logger

Logger = logger.MyLogger("extends").getLogger()


class animal(object):
    name = None
    age = None
    type_ = None
    __private = None

    def __init__(self, type_, name=None, age=None,private = "hi"):
        Logger.info(type(self))
        self.name = name
        self.age = age
        self.type_ = type_
        self.__private = private

    def __repr__(self):
        return ("type_={},name={},age={}", self.type_, self.name, self.age)

    def set_name(self, name):
        Logger.info(type(self))
        self.name = name

    def sleep(self):
        Logger.info("睡了一小时，舒服！")

    def change_type(self, type):
        self.type_ = type


class koala(animal):

    def __init__(self, name=None, age=None):
        Logger.info(type(self))
        super().__init__(type_="koala")

    def sleep(self):
        Logger.info("睡了一天，真香！")


hyn = koala(name="hynkoala", age=5)
Logger.info(type(hyn))
hyn.sleep()
hyn.set_name("wangcegnling")

"""
字类继承所有父类的属性和方法,(私有方法和属性除外)，同样字类也能重写父类的属性和方法
所有字类对象调用父类的方法时，进入方法后的self参数均表示当前对象自己
复写父类的构造方法（否则字类和父类有何区别？），有两种方式：
    1：animal.__init__(self,type_="koala")
    2: super().__init__(type_="koala")
"""
