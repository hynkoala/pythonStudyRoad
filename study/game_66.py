# @datetime: 7/31 0031
# @author: hanyaning@deri.energy
# @description:

import random

a = []
from util import logger, commonUtil

Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()

# 1_0.06,2_0.66;3_6.66;4_66.66;5_666.66;6_6666.66;

#
# for times in range(6):
#     times = times+1
#     loop_n_list = []
#     for test in range(100):
#
#         start_time = time.time()
#         loop_n = 0
#         while True:
#
#             n=0
#             i=0
#             while i<times:
#                 i+=1
#                 if 6 == random.randint(1,6):
#                     n+=1
#                     continue
#                 break
#             loop_n+=1
#             if n==times:
#                 loop_n_list.append(loop_n)
#                 break
#         # Logger.info(f"连续{times}次摇出6共尝试{loop_n}次，摇{do_n}次筛子，用时{time.time()-start_time}")
#     Logger.info(f"连续出现{times}次最少尝试{min(loop_n_list)}，平均尝试{numpy.mean(loop_n_list)}")

# get = 0
# put = 0
# result_dict = {}
# for i in range(6):
#     result_dict[i+1] = 0
# for times in range(6):
#     times = times+1
#     loop_n_list = []
#     for test in range(100):
#
#         start_time = time.time()
#         loop_n = 0
#         while True:
#             n=0
#             i=0
#             while i<6:
#                 i+=1
#                 if 6 == random.randint(1,6):
#                     n+=1
#                     if n>times:
#                         break
#             loop_n+=1
#
#             if n==times:
#                 loop_n_list.append(loop_n)
#                 break
#
#     # Logger.info(f"连续{times}次摇出6共尝试{loop_n}次，摇{do_n}次筛子，用时{time.time()-start_time}")
#     Logger.info(f"出现{times}次最少尝试{min(loop_n_list)}，平均尝试{numpy.mean(loop_n_list)}")

"""
规则：三次起摇
3次：0.36
4-5次：0.66*次数
"""
get = 0
put = 0
result_dict = {}
for i in range(7):
    result_dict[i] = 0
times = 50000
rock_times = 6
for test in range(times):

    n = 0
    i = 0
    while i < rock_times:
        i += 1
        if 6 == random.randint(1, 6):
            n += 1
    result_dict[n] += 1
get = 6.66 * times
for key,value in result_dict.items():
    if key ==1:
        # 不太6
        put += 0.06*value
    if key == 2:
        # 有点6
        put += 0.66*value
    if key == 3:
        # 非常6
        put +=6.66*value
    if key == 4:
        # 花式6
        put +=66.66*value
    if key == 5:
        # 惊天6
        put +=666.66*value
    if key == 6:
        # 爆炸6
        put +=6666.66*value

Logger.info(f"{result_dict}")
Logger.info(f"get{get}put{put}")