# @datetime: 7/26 0026
# @author: hanyaning@deri.energy
# @description: 使用mysql进行增删改查
import pymysql

from util import logger, commonUtil

Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()

conn = pymysql.connect(
    host="127.0.0.1",
    port=3306,
    user="koala",
    password="koala",
    database="test",
    charset="utf8"
)
# 得到一个可以执行sql的光标对象
cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)

# 查
query_user_sql = "select * from user"
cursor.execute(query_user_sql)
# 获取所有查询记录
users = cursor.fetchall()
# 查询单条数据用fetchone()
Logger.info(users)

# 删
del_sql = "delete from user where name='hyn'"
try:
    cursor.execute(del_sql)
    conn.commit()
    Logger.info("删除成功")
    cursor.execute(query_user_sql)
    # 获取所有查询记录
    users = cursor.fetchall()
    # 查询单条数据用fetchone()
    Logger.info(users)
except Exception:
    conn.rollback()

# 增
insert_sql = "insert into user(id,name) values (%s,%s)"
data = [("1","hyn"),("2","wcl")]
try:
    cursor.executemany(insert_sql,data)
    conn.commit()
    Logger.info("增加成功")
    cursor.execute(query_user_sql)
    # 获取所有查询记录
    users = cursor.fetchall()
    # 查询单条数据用fetchone()
    Logger.info(users)
except:
    conn.rollback()
    Logger.info("增加失败")

# 改
update_sql = "update user set id = '1' where name='wcl'"
try:
    cursor.execute(update_sql)
    conn.commit()
    Logger.info("更新成功")
    cursor.execute(query_user_sql)
    # 获取所有查询记录
    users = cursor.fetchall()
    # 查询单条数据用fetchone()
    Logger.info(users)
except:
    conn.rollback()

cursor.close()
conn.close()
