# @datetime: 7/25 0025
# @author: hanyaning@deri.energy
# @description:
import time

import wx

from util import logger, commonUtil

Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()

# pip install wxPython
plat_data_path = ""
excel_data_path = ""


class MyFrame(wx.Frame):

    def __init__(self, title, pos=(0, 0), size=(600, 400)):
        wx.Frame.__init__(self, None, wx.ID_ANY, title=title, pos=pos, size=size)
        panel = wx.Panel(self, wx.ID_ANY)
        btn_choose_excel = wx.Button(panel, label="选择检测数据", pos=(10, 10),id=1)
        excel_process_btn = wx.Button(panel, label="处理excel数据", pos=(120, 10),id=9)
        btn_choose_plat_data = wx.Button(panel, label="选择平台数据", pos=(10, 50),id=2)
        btn_choose_excel.Bind(wx.EVT_BUTTON, self.choose_file)
        btn_choose_plat_data.Bind(wx.EVT_BUTTON, self.choose_file)
        excel_process_btn.Bind(wx.EVT_BUTTON, self.excel_process)

    def choose_file(self, event):
        dlg = wx.DirDialog(
            self, message="选择路径",

        )
        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPath()
            if event.Id==1:
                excel_data_path = paths
            elif event.Id ==2:
                plat_data_path = paths
            Logger.info(paths)

    def excel_process(self,event):
        pass
def sayHello(event):
    msg_dialog = wx.MessageDialog(parent=frame,message="hello world",caption="提示",style=wx.YES_NO)
    if msg_dialog.ShowModal() == wx.ID_YES:
        text.SetValue("Hello world")

    Logger.info("Hello world")
if __name__ == "__main__":
    app = wx.App()
    frame = wx.Frame(None,title="充电数据比对工具")
    btn = wx.Button(frame,label='button',id=1)
    text = wx.TextCtrl(frame,id=2,pos=(100,0))
    btn.Bind(wx.EVT_BUTTON,sayHello)

    frame.Show()
    app.MainLoop()

"""笔记
打开文件方式：
    1、打开文件：wx.FileDialog(frame,message="",style="wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR")
    2、选择文件夹：wx.DirDialog(frame,message="")
组件：
    按钮：wx.Button(frame,label='按钮1',id=1)
    文本框：wx.TextCtrl(frame,id=2) 后续通过.SetText()进行填充内容
    进度条：wx.ProgressDialog()
    对话框：wx.MessageDialog(message="？？？",caption="提示",style="wx.YES_NO|WX.ICOM_AUTO_NEEDED"),后续须要加.ShowModel()==wx.ID_YES条件判断
    
退出：
    wx.Exit()
"""