# @datetime: 7/19 0019
# @author: hanyaning@deri.energy
# @description:
"""
爬虫的相关工具，包括html下载器
"""
import json
import time

import requests

from util import logger, commonUtil

Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()


class HtmlDownloader(object):
    __type_get = "get"
    __type_post = "post"
    __default_delay = 5

    def __init__(self, request_header=None, request_cookies=None, proxies=None, delay=False,turn_to_json = False):
        """html页面下载器，通过requests模块的request方法进行页面下载，返回response.text

        :param request_header:
        :param request_cookies:
        :param proxies:
        :param delay: 是否设置延迟，可传入boolean或者int类型，为True时默认值为
        :param turn_to_json: 结果是否转换为json
        """

        self.request_header = request_header
        self.request_cookies = request_cookies
        self.proxies = proxies
        self.turn_to_json = turn_to_json
        if delay and type(delay) != int:
            global __default_delay
            self.delay = __default_delay
        else:
            self.delay = delay

    def download(self, url, type=__type_get, data=None):
        """执行下载方法，内部调用requests模块的request方法

        :param url: url地址
        :param type: 访问方式，post或者get
        :param data: 数据，json或者dict格式
        :return: 返回reponse.text
        """

        if self.delay:
            time.sleep(self.delay)
        if type == "post":
            response = requests.request(headers=self.request_header, method=type, cookies=self.request_cookies, url=url,
                                        data=data, proxies=self.proxies)
        else:
            response = requests.request(headers=self.request_header, method=type, cookies=self.request_cookies, url=url,
                                        params=data, proxies=self.proxies)
        response.encoding = 'utf-8'
        code = response.status_code
        html = response.text

        # code不为200则请求失败
        if code != 200:
            Logger.error("请求失败，code为：%s", code)
            Logger.error(response.text)
            return None
        if self.turn_to_json:
            return json.loads(html)
        return html

    def get(self, url, params):
        """调用download方法，传入type = "get"

        :param url:
        :param params:
        :return:
        """
        return self.download(url=url, type="get", data=params)

    def post(self, url, data):
        """ 调用download方法，传入type="post"

        :param url:
        :param data:
        :return:
        """
        return self.download(url=url, type="post", data=data)


def chrome_header_parse(str):
    """ 由于chrome浏览器F12看到的header不是标准的json或字典形式，将其转换为字典返回

    :param str: 输入字符传
    :return: dict
    """
    result = {}

    key_values = str.split("\n")
    for key_value in key_values:
        if commonUtil.isBlank(key_value):
            continue
        key_value = key_value.strip().split(":")
        result[key_value[0].strip()] = key_value[1].strip()
    return result
