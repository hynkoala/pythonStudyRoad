# @datetime: 7/26 0026
# @author: hanyaning@deri.energy
# @description: 多进程创建及管理工具
import multiprocessing
import os
from multiprocessing import Process,cpu_count,Pool
# from multiprocessing.pool import Pool

from util import logger, commonUtil

"""readme
工具名称：
    多进程创建及管理工具
工具的功能：
    创建及管理多进程
工具的使用：
    1、
"""
Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()


class MyProcess(Process):
    def __init__(self, size=10):
        super().__init__()


# class MyPool(Pool):
#     def __int__(self, size=1):
#         cpu_size = cpu_count()
#         if commonUtil.isBlank(size) or size > cpu_size:
#             size = cpu_size
#         super().__init__(processes=size)

# def getPool(size:int):
#     cpu_size = cpu_count()
#     if commonUtil.isBlank(size) or size > cpu_size:
#         size = cpu_size - 1
#     return Pool(processes=size)
def hello(msg):
    print(msg)
    Logger.info(os.getpid())
if __name__=="__main__":
    pool = Pool(processes=10)
    Logger.info(os.getpid())
    while True:
        pool.apply_async(hello,"hello world")
        break
    pool.close()
    pool.join()
    Logger.info(os.getpid())