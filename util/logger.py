import logging
import os.path
import time

"""readme
工具名称：
    日志记录工具
工具的功能：
    实现日志记录到控制台或者文本
工具的使用：
    1、创建logger对象，此处提供两个对象的创建
        1.1、创建默认的对象，同时打印到控制台和记录文本
        Logger = MyLogger("log_name").getLogger()
        1.2、创建控制台打印对象，不输出文本
        Logger = Console("log_name").getLogger()
        1.3、通过参数控制
        MyLogger类保留了一个参数output，可设值为：both，file，console，控制日志的输出方式
    2、使用对象输出日志
        Logger.info("hello world")
    3、查看日志文件（如果有）
        日志文件默认保存在工程目录下logs文件夹下
"""


class MyLogger(logging.Logger):
    """自定义logger对象，继承自logging.Logger，实现文件和控制台的输出"""
    # 首先重建一个logger对象
    __logger = None

    def __init__(self, name="logger", output="both", level=logging.DEBUG, console_level=logging.INFO, mode='a'):
        self.__logger = logging.getLogger(name)
        # 设置logger的等级
        super().__init__(name)
        # 等级的设定既可以直接设置大写的英文，也可以设置为logging模块的内置属性，python会自动进行转换判断
        # 这里设置的是全局的level，后面可根据输出到文件和控制台设置相应的level
        # 注意这各会设置最低的等级，后续的设置只能比这个高
        self.__logger.setLevel(level)
        formatter = logging.Formatter("%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")

        if output in ("both", "file"):
            # 组织一个带时间戳的字符串作为日志文件的名字,实现每天记录一个日志文件
            date_time = time.strftime("%Y%m%d", time.localtime(time.time()))
            log_path_str = os.path.join(os.path.abspath(os.path.join(os.getcwd(), "")), "logs")
            # python 在创建fileHandler时路径不存在会报FileNotFoundError，这里要新建下路径（而具体文件存不存在都时可以的，python会自动创建文件）
            if not os.path.exists(log_path_str):
                os.makedirs(log_path_str)

            log_name = os.path.join(log_path_str, date_time + '.log')
            # 创建一个logging输出到文件的handler并设置等级和输出格式
            # mode属性用于控制写文件的模式，w模式每次程序运行都会覆盖之前的logger，而默认的是a则每次在文件末尾追加
            fh = logging.FileHandler(log_name, mode)
            fh.setLevel(level)
            fh.setFormatter(formatter)
            # 给logger对象添加handler
            self.__logger.addHandler(fh)
            fh.close()
        if output in ("both", "console"):
            # 如果需要同时输出到控制台
            ch = logging.StreamHandler()
            ch.setFormatter(formatter)
            ch.setLevel(console_level)
            self.__logger.addHandler(ch)
            ch.close()

    def getLogger(self):
        return self.__logger

    @property
    def name(self):
        return self.__logger.name

    @name.setter
    def name(self, name):
        self.__logger.name = name


class ConsoleLogger(MyLogger):
    def __init__(self, name="logger", level=logging.DEBUG):
        super().__init__(name=name, level=level, output="console")
