# @datetime: 7/26 0026
# @author: hanyaning@deri.energy
# @description: mysql数据库快速使用工具，主要使用pymysql模块
import os
from typing import List, Any, Dict

import attr
import pymysql

from util import logger, commonUtil, propertiesUtil
from util.exception.sqlException import *

Logger = logger.ConsoleLogger(commonUtil.get_file_name(__file__)).getLogger()
"""readme
工具的功能：
    连接mysql数据库并执行sql操作
工具的使用：
    1、创建mysql数据库连接器
    database = MyMysql(user="koala",password="koala",database="test"),默认读取config/mysqlConfig.properties
    2、创建sql执行器
    executor = SqlExecutor(database)
    或者
    entity_mapper = EntityMapper(database)_针对实体对象设计
    3、执行sql
    executor.select(table_name,cols(查询列),condition(查询条件，例如：where 1=1))
    executor.update()
    executor.insert()
    executor.delete()
    或者
    entity_mapper.select_model(table_name,**kwargs(查询条件，以字典形式传入，自动进行拼接where子句，只能通过and连接，只支持=和in的比较))
    entity_mapper.update_by_model(model,table_name,not_null(是否只更新model中的非空值)，**kwargs)
    entity_mapper.update_many()_多次调用update_by_model
    4、销毁执行器，同时会自动断开数据库连接，之前会自动提交一次事务
    executor.destory()
"""


@attr.s(init=True)
class MyMysql():
    # 数据库连接器，传入数据库名和密码连接数据库
    config_file_path = os.path.join(os.getcwd(), "config/mysqlConfig.properties")
    # if os.path.exists(config_file_path):
    #     mysql_properties = propertiesUtil.Properties(file_name=config_file_path)
    #     host = attr.ib(default=mysql_properties.get("host"), init=True)
    #     port = attr.ib(default=int(mysql_properties.get("port")), init=True)
    #     user = attr.ib(default=mysql_properties.get("user"), init=True)
    #     password = attr.ib(default=mysql_properties.get("password"), init=True)
    #     database = attr.ib(default=mysql_properties.get("database"), init=True)
    #     charset = attr.ib(default=mysql_properties.get("charset"), init=True)
    # else:
    #     host = attr.ib(default="127.0.0.1", init=True)
    #     port = attr.ib(default=3306, init=True)
    #     user = attr.ib(default=None, init=True)
    #     password = attr.ib(default=None, init=True)
    #     database = attr.ib(default=None, init=True)
    #     charset = attr.ib(default="utf8", init=True)

    host = attr.ib(default="127.0.0.1", init=True)
    port = attr.ib(default=3306, init=True)
    user = attr.ib(default=None, init=True)
    password = attr.ib(default=None, init=True)
    database = attr.ib(default=None, init=True)
    charset = attr.ib(default="utf8", init=True)


    def generate_conn(self):
        conn = pymysql.connect(host=self.host, port=self.port, user=self.user, password=self.password,
                               database=self.database, charset=self.charset)
        return conn

    def generate_cursor(self, conn, cursor=None):
        cursor = conn.cursor(cursor)
        return cursor


class SqlExecutor():
    """
    sql执行器，传入数据库对象后提供增删改查方法
    """

    def __init__(self, database=MyMysql(), cursor_type=pymysql.cursors.DictCursor, auto_close=False):

        self.conn = database.generate_conn()
        self.cursor = database.generate_cursor(self.conn, cursor=cursor_type)
        self.auto_close = auto_close

    def destory(self):
        """
        使用完成时销毁执行器，实际上是关闭cursor和conn
        :return:
        """
        self.commit()
        self.cursor.close()
        self.conn.close()

    def select(self, table_name, cols: List[str] = None, condition: str = None):
        """单表查询

        :param table_name: 表名
        :param cols: 定义查询列，默认为*
        :param condition: 指定查询条件
        :return: dict查询结果集
        """
        select_cols = "*"
        if cols:
            select_cols = ",".join(cols)
        sql = f"select {select_cols} from {table_name}"
        if condition:
            sql += " " + condition
        self.execute(sql)
        return self.cursor.fetchall()

    def insert(self, table_name, cols_values: Dict[str,Any]):
        """插入数据

        :param table_name: 表明
        :param cols_values: 插入元数据字典，key为表中字段，value为要插入的值
        :return:
        """
        cols = cols_values.keys()
        values = cols_values.values()
        sql = f"insert into {table_name}({','.join(cols)}) values ({commonUtil.join_collection_by(values,',',add_quote_marks = True)})"
        return self.execute(sql)

    def update(self, table_name, cols_values: Dict[str,Any], condition):
        """更新的方法

        :param table_name: 表名
        :param cols_values: 要更新的列与值
        :param condition: 更新条件
        :return:
        """

        cols = cols_values.keys()
        values = cols_values.values()
        if commonUtil.isBlank(condition):
            raise SqlConditionMissingError
        if not cols_values:
            raise ArgumentMissingError(method="update", missing_args="cols_values")
        sql = f"update {table_name} set "

        for key, value in cols_values.items():
            if value is None:
                value = ""
            elif type(value) == int or type(value) == float:
                sql += key + "=" + str(value) + ","
            else:
                sql += key + "='" + str(value) + "',"
        sql = sql.strip(",")
        sql += " " + condition
        return self.execute(sql)

    def delete(self, table_name, condition):
        """删除的方法

        :param table_name: 表名
        :param condition: 条件
        :return:
        """
        if commonUtil.isBlank(condition):
            raise SqlConditionMissingError("删除操作必须传入条件限制！")
        sql = f"delete from {table_name}{condition}"
        return self.execute(sql)

    def execute(self, sql):
        """公用的执行方法

        :param sql:
        :return:
        """
        Logger.info(sql)

        self.cursor.execute(sql)
        return 1

    def commit(self):
        """提交事务

        :return:
        """
        self.conn.commit()

    def rollback(self):
        """事务回滚

        :return:
        """
        self.conn.rollback()


class EntityMapper(SqlExecutor):
    def __init__(self, database=MyMysql(), cursor_type=pymysql.cursors.DictCursor, auto_close=False):
        super().__init__(database=database, cursor_type=cursor_type, auto_close=auto_close)

    def select_model(self, table_name, **kwargs):
        """依表名和条件进行单表查询，支持=和in的查询

        :param table_name: 表名
        :param kwargs: 查询条件，
        :return:
        """
        condition = combine_condition(**kwargs)
        return self.select(table_name= table_name,condition = condition)


    def update_by_model(self, model: object, table_name, not_null=False, **kwargs):
        """更新的方法

        :param model: 带更新对象
        :param table_name: 对应数据库表名
        :param not_null: model中值为空时是否进行更新，默认更新所有值
        :param kwargs: 标识字段，用于生成where子句
        :return: 0或1
        """
        if commonUtil.isBlank(kwargs):
            # 不允许全表进行更新，必须设置条件
            raise SqlConditionMissingError("更新语句缺少限制字段")
        model_dict = model.__dict__
        sql = "UPDATE " + table_name + " SET "
        effective_sql = False
        for key, value in model_dict.items():
            if not not_null or value:
                effective_sql = True
                if value is None:
                    value = ""
                if type(value) == int or type(value) == float:
                    sql += key + "=" + str(value) + ","
                else:
                    sql += key + "='" + str(value) + "',"
        if effective_sql:
            sql = sql.strip(",")
            sql += combine_condition(**kwargs)
            return self.execute(sql)

    def update_many(self, models: list, table_name, keys, not_null=False):
        if commonUtil.isBlank(models):
            return 1

        if not keys:
            raise SqlConditionMissingError("更新方法key值缺失")

        model = models[0]

        for key in keys:
            if not hasattr(model, key):
                raise SqlConditionNotExistError("key值非表中字段")
        for model in models:
            where_condition = {}
            for key in keys:
                where_condition[key] = getattr(model, key)
            self.update_by_model(model, table_name=table_name, not_null=not_null, **where_condition)


def combine_condition(**kwargs):
    """组织查询条件，传入的字段值为tuple或者list时，用in连接，否则用=连接

    :param kwargs:
    :return: 适用于sql中的条件字符串
    """
    condition = " where 1=1"
    for key, value in kwargs.items():
        if commonUtil.isNotBlank(value):
            if type(value) in (tuple, list):
                condition += " and " + key + " in " + str(tuple(value))
            else:
                condition += " and " + key + "=" + value
    return condition
