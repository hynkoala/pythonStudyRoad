# @datetime: 7/17 0017
# @author: hanyaning@deri.energy
# @description: 记录日志的装饰器
import socket
from datetime import datetime

import redis

conn = redis.Redis(host="127.0.0.1", port="6379")


class Log(object):

    def __init__(self):
        self.date = datetime.now()
        self.ip = socket.gethostname()
        self.func_param_list = []
        self.func_param_dict = {}
        self.func_result: None

    @staticmethod
    def log(save_param=True, save_result=True,comment = None):
        def do_function(func):
            def wrapper(*args, **kwargs):
                log_model = Log()
                log_model.func_name = func.__name__
                result_data = func(*args, *kwargs)
                if save_param:
                    log_model.func_param_list.extend(args)
                    log_model.func_param_dict = kwargs
                if save_result:
                    log_model.func_result = result_data
                if comment:
                    log_model.comment = comment

                key = "python_study_road_project."+"func.__name__"+str(log_model.date.timestamp())
                print(key)
                # 存入redis
                conn.set(key, str(log_model.__dict__))
                return result_data

            return wrapper

        return do_function
