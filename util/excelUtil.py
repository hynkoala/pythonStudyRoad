import os.path
import time

import pandas as pd

from util import logger

LOGGER = logger.MyLogger("excelUtils").getLogger()

"""readme
工具名称：
    excel工具类
工具的功能：
    excel的读写
工具的使用：
    1、读取
        1.1、创建对象
            reader = ExcelReader(path="")
        1.2、读取数据
            data = reader.getData()
    2、保存
        2.1、创建对象
            saver =ExcelSaver(to_path="",file_name="",file_suffix=".xlsx")
        2.2、保存数据
            提供两种格式数据的保存
            1、list(dict)格式
                saver.saveDictArray(data)
            2、dataFrame格式
                saver.saveDataFrame(data)
        
"""
class ExcelReader:
    """
    读取excel工具
    """

    def __init__(self, path, file_suffix=".xls", sort_by=None):
        self.path = path
        self.file_suffix = file_suffix
        self.sort_by = sort_by

    def setPath(self, path, file_suffix=".xls", sort_by=None):
        self.__init__(path, file_suffix=file_suffix, sort_by=sort_by)

    def getData(self, sheet_name=0, skiprows=0, skipfooter=0, to_records=False):
        """

        :param sheet_name: 指定读取的sheet页，可设置为int或str
        :param skiprows: 跳过最上方的行数
        :param skipfooter: 跳过最底部的行数
        :param to_records: 结果是否转换为list(dict)形式，默认为DataFrame格式
        :return:
        """
        if not os.path.exists(self.path):
            raise FileNotFoundError()
        data = pd.DataFrame()
        if os.path.isfile(self.path):
            data = pd.read_excel(self.path, sheet_name=sheet_name, skiprows=skiprows, skipfooter=skipfooter)
        elif os.path.isdir(self.path):
            xls_names = [x for x in os.listdir(self.path) if x.endswith(self.file_suffix)]
            for xls_name in xls_names:
                df = pd.read_excel(os.path.join(self.path, xls_name), sheet_name=sheet_name, skiprows=skiprows,
                                   skipfooter=skipfooter)
                data = data.append(df, sort=False)
            LOGGER.info("读取Excel文件完毕，共读取" + str(xls_names.__len__()) + "个文件")
        if self.sort_by:
            data.sort_values(by=self.sort_by, inplace=True)
        if to_records:
            return data.to_dict(orient="records")
        return data


class ExcelSaver:
    """
    保存excel工具
    """

    def __init__(self, to_path, file_suffix=".xlsx", file_name=None):
        self.to_path = to_path
        self.file_suffix = file_suffix
        self.file_name = file_name

    # 保存字典list为excel
    def saveDictArray(self, data):
        """
        传入数据保存为excel
        :param data: 字典list
        :return:
        """
        path = self.combinePath()
        data_frame = pd.DataFrame.from_records(data)
        writer = pd.ExcelWriter(path, engin='openpyxl')
        data_frame.to_excel(excel_writer=writer, index=None)
        writer.save()
        writer.close()

    def saveDataFrame(self, data_frame):
        path = self.combinePath()
        writer = pd.ExcelWriter(path, engin='openpyxl')
        data_frame.to_excel(excel_writer=writer, index=None)
        writer.save()
        writer.close()

    def combinePath(self):
        path = self.to_path
        if not os.path.exists(path):
            os.makedirs(path)
        if self.file_name:
            file_name = self.file_name
        else:
            file_name = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        path = os.path.join(path, file_name + self.file_suffix)
        return path
