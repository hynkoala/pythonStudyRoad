# @datetime:7/4/0004
"""
通用工具类
"""
import operator
from typing import Any

import numpy

__author__ = "hanyaning@deri.energy"


def isBlank(obj: Any, number=False):
    """
    判断传入的对象是否是空

    :param obj: 带判断对象
    :param number: 是否依数字判断，0认为是blank
    :return: True or False
    Examples
    ------
    >>> isBlank(None)
    True
    >>> isBlank([]) or isBlank({})
    True
    >>> isBlank('      ')
    True

    """
    if obj is None:
        return True
    if isinstance(obj, int) or isinstance(obj, float):
        if numpy.isnan(obj):
            return True
        if number and obj == 0:
            return True
    elif isinstance(obj, str):
        object = obj.strip()
        if object == "":
            return True
    elif len(obj) == 0:
        return True
    return False


def isNotBlank(object, number=False):
    """
    isBlank取反
    :param object:
    :return:
    """
    return not isBlank(object, number)


def get_file_name(file, separate="/"):
    if file:
        return str(file).split(separate)[-1].split(".")[0]


def get_file_suffix(file):
    if file:
        return str(file).split(".")[-1].strip()


def formatEquationToDict(str, separate=";"):
    """
    将等式形式的key=value字符串转换为dict
    :param str:
    :return: dict()
    example: "name=hyn;age=25"->{"name":"hyn","age":25}
    """
    str = str.strip()
    if str[len(str) - 1] == separate:
        str = str[0:len(str) - 1]
    key_values = str.split(separate)
    result = {}
    for key_value in key_values:
        result[key_value.split("=")[0].strip()] = key_value.split("=")[1].strip()
    return result


def narrow_data_by_dichotomy(data, key, value, equals_func=None, value_handler=None):
    """
    二分法查数据

    :param data: list(dict)
    :param key: 目标key值
    :param value: 目标value
    :param func: 判断相等的函数，可自定义传入，默认用==判断
    :return:
    """
    # 对data按照key进行排序
    data = sorted(data, key=operator.itemgetter(key))
    # data的开始到结束间隔大小
    begin = 0
    end = len(data)
    # 每次循环后的data对象的长度，长度已经小于等于2时终止循环并返回最后的三条数据
    interval = end - begin
    if equals_func is None:
        def equals(a, b):
            return a == b

        equals_func = equals
    if value_handler is None:
        def value_func(x):
            return x

        value_handler = value_func
    while interval > 2:
        middle_index = begin + int(interval / 2)
        middle_value = value_handler(data[middle_index][key])
        # 存在数据量特别大情况下有多条value很相近的数据，应该一并返回
        if equals_func(value, middle_value):
            begin = middle_index
            end = middle_index + 1
            # 当前点向前检查是否还有endTime相同的点
            while begin - 1 >= 0:

                front_value = value_handler(data[begin - 1][key])
                if not equals_func(value, front_value):
                    break
                else:
                    begin -= 1
            # 当前点向后检查是否还有value相同的点
            while end < len(data):
                later_value = value_handler(data[end][key])
                if not equals_func(value, later_value):
                    break
                else:
                    end += 1
            return data[begin:end]
        if value > middle_value:
            begin = middle_index
        else:
            end = middle_index
        interval = end - begin
    result = []
    for d in data[begin:end]:
        if d[key] == value:
            result.append(d)
    return result


def dict_to_obj(target_dict: dict, obj: object, ignore_case=False, only_null_value=False):
    """字典数据放入对象中

    :param target_dict: 目标字典
    :param obj: 目标对象
    :param ignore_case: 是否忽略大小写
    :param only_null_value: 是否只更新空的字段
    :return: 返回传入的obj
    """
    obj_keys = dir(obj)
    if ignore_case:
        obj_keys = [x.lower() for x in dir(obj)]

    for key, value in target_dict.items():
        if ignore_case:
            key = key.lower()
        if key in obj_keys:
            if only_null_value and isNotBlank(getattr(obj, key), number=True):
                continue
            setattr(obj, key, value)
    return obj


def join_collection_by(target_collection, by=",", add_quote_marks=False, quote_mark="'"):
    """对集合中的元素连接成字符串

    :param target_collection: 目标集合
    :param by: 连接的符号
    :param add_quote_marks: 处理sql值时使用，给value两边添加单引号
    :return: str
    """
    result = ""
    for value in target_collection:
        if value:
            if not add_quote_marks:
                result += str(value) + by
            else:
                result += quote_mark + str(value) + quote_mark + by
    return result.strip(by)
