# @datetime: 7/31 0031
# @author: hanyaning@deri.energy
# @description:
class SqlException(Exception):
    """
    sql异常基类
    """
    def __init__(self,message):
        super().__init__()
        self.error_info = message

    def __str__(self):
        return self.error_info

class ArgumentMissingError(SqlException):
    """
    方法缺失参数
    """
    def __init__(self,message="方法缺失参数，执行数据库操作失败",method = None,missing_args = None):
        super().__init__(message)
        self.method = method
        self.missing_args = missing_args

    def __str__(self):
        return f"{self.error_info}(方法名：{self.method};缺失参数：{self.missing_args})"
class SqlConditionError(SqlException):
    """
    更新语句where子句key值指定异常
    """
    pass

class SqlConditionMissingError(SqlConditionError):
    """
    更新语句where子句key值未指定，不允许进行全表更新，必须添加限制
    """
    pass

class SqlConditionNotExistError(SqlConditionError):
    """
    更新语句where子句指定的key值非表中字段
    """
    pass

